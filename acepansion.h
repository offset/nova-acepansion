/*
** nova.acepansion public API
*/

#ifndef ACEPANSION_H
#define ACEPANSION_H


#define LIBNAME "nova.acepansion"
#define VERSION 1
#define REVISION 3
#define DATE "21.10.2023"
#define COPYRIGHT "� 2021-2023 Philippe Rimauro"

#define API_VERSION 7


#endif /* ACEPANSION_H */

