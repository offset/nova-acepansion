@DATABASE nova.acepansion.guide
@$VER: Nova 1.3 (21.10.2023)
@INDEX Main
@TOC Main
@AUTHOR Philippe Rimauro
@MASTER ACE.guide

@NODE Main
@TITLE "ACE � Nova expansion plugin"


    @{b}Nova expansion plugin 1.3@{ub}
    Plugin to emulate the Nova from PulkoTronics.

    Copyright � 2021-2023 Philippe Rimauro
    All rights reserved.


    @{u}Description@{uu}

    This plugin emulates non volatile memory (NV-RAM) and real time clock (RTC)
    of the Nova from PulkoTronics.

    The clock is automatically synchronized with the one from the host computer.
    The contents of the non volatile memory is stored in a file "nova.nvram"
    located in the "Plugins" drawer. It is loaded at plugin activation and saved
    at desactivation. If the file does not exist at plugin activation, then the
    Nova card is initialized with an empty memory.


    @{u}Tooltypes@{uu}

    None.


   @{u}Credits@{uu}

     @{b}Code:@{ub}
        @{b}*@{ub} Philippe @{i}'OffseT'@{ui} Rimauro - @{"mailto:offset@cpcscene.net" system "openurl mailto:offset@cpcscene.net"}
     @{b}Icon:@{ub}
        @{b}*@{ub} Christophe @{i}'Highlander'@{ui} Delorme - @{"mailto:chris.highlander@free.fr" system "openurl mailto:chris.highlander@free.fr"}
     @{b}Documentation:@{ub}
        @{b}*@{ub} Philippe @{i}'OffseT'@{ui} Rimauro - @{"mailto:offset@cpcscene.net" system "openurl mailto:offset@cpcscene.net"}
     @{b}Translations:@{ub}
        @{b}*@{ub} Philippe @{i}'OffseT'@{ui} Rimauro - @{"mailto:offset@cpcscene.net" system "openurl mailto:offset@cpcscene.net"}
        @{b}*@{ub} Stefan @{i}'polluks'@{ui} Haubenthal - @{"mailto:polluks@sdf.lonestar.org" system "openurl mailto:polluks@sdf.lonestar.org"}
        @{b}*@{ub} Juan Carlos Herran Martin - @{"mailto:juancarlos@morguesoft.eu" system "openurl mailto:juancarlos@morguesoft.eu"}

@ENDNODE

