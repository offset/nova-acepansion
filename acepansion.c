/****** nova.acepansion/background ******************************************
*
* DESCRIPTION
*  This plugin provides Nova emulation to ACE CPC Emulator.
*
* HISTORY
*
*****************************************************************************
*
*/

#include <clib/alib_protos.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>
#include <proto/timer.h>

#include <dos/dos.h>
#include <utility/utility.h>

#define USE_INLINE_API
#include <acepansion/lib_header.h>
#include <libraries/acepansion_plugin.h>

#include "acepansion.h"
#define CATCOMP_NUMBERS
#include "generated/locale_strings.h" /* catalog string ids */



#define NVRAM_FILE  "Plugins/nova.nvram"
#define NVRAM_SIZE  0x8000
#define NVRAM_OFF   -1

#define IDX_CLOCK_MODE  (NVRAM_SIZE-8)
#define IDX_CLOCK_SEC   (NVRAM_SIZE-7)
#define IDX_CLOCK_MIN   (NVRAM_SIZE-6)
#define IDX_CLOCK_HOUR  (NVRAM_SIZE-5)
#define IDX_CLOCK_WDAY  (NVRAM_SIZE-4)
#define IDX_CLOCK_MDAY  (NVRAM_SIZE-3)
#define IDX_CLOCK_MONTH (NVRAM_SIZE-2)
#define IDX_CLOCK_YEAR  (NVRAM_SIZE-1)

struct PluginData
{
    // Plugin generic part (do NEVER put anything before this!)
    struct ACEpansionPlugin common;

    // Nova specifics
    UBYTE   nvram[NVRAM_SIZE];
    USHORT  map;
    LONG    adr;
};



static const UBYTE ToBCD[] =
{
    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,
    0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,
    0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,
    0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,
    0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,
    0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,
    0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,
    0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,
    0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,
    0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99
};

static const UBYTE ToMDay[] = { 7, 1, 2, 3, 4, 5, 6 };




struct Library *DOSBase = NULL;
struct Library *UtilityBase = NULL;
struct Device *TimerBase = NULL;

static struct MsgPort *msgPort = NULL;
static struct IORequest *timerReq = NULL;



static VOID updateClockRegisters(struct PluginData *myPlugin);



/*
** Private function which is called to initialize commons
** just after the library was loaded into memory and prior
** to any other API call.
**
** This is the good place to open our required libraries.
*/
BOOL InitResources(VOID)
{
    DOSBase = OpenLibrary(DOSNAME, 0L);
    UtilityBase = OpenLibrary(UTILITYNAME, 0L);

#if defined(__amigaos__)
    if((msgPort = CreateMsgPort()))
    {
        if((timerReq = CreateIORequest(msgPort, sizeof(struct timerequest))))
        {
            if(OpenDevice(TIMERNAME, UNIT_MICROHZ, (struct IORequest *)timerReq, 0) == 0)
            {
            	TimerBase = ((struct timerequest *)timerReq)->tr_node.io_Device;
            }
        }
    }
#else
    TimerBase = (struct Device *)OpenLibrary(TIMERNAME, 0L);
#endif

    return DOSBase != NULL && UtilityBase != NULL && TimerBase != NULL;
}



/*
** Private function which is called to free commons
** just before the library is expurged from memory
**
** This is the good place to close our required libraries.
*/
VOID FreeResources(VOID)
{
    CloseLibrary(DOSBase);
    CloseLibrary(UtilityBase);

#if defined(__amigaos__)
    if(msgPort)
    {
        if(timerReq)
        {
            if(TimerBase)
            {
                CloseDevice(timerReq);
            }
            DeleteIORequest(timerReq);
        }
        DeleteMsgPort(msgPort);
    }
#else
    CloseLibrary((struct Library *)TimerBase);
#endif
}



/****** nova.acepansion/CreatePlugin ****************************************
*
* NAME
*   CreatePlugin -- (V1) -- GUI
*
* SYNOPSIS
*   struct ACEpansionPlugin * CreatePlugin(CONST_STRPTR *toolTypes, struct SignalSemaphore *sema)
*
* FUNCTION
*   Create the plugin data structure and configure how it will be handled by
*   ACE. This function is usually called at ACE startup.
*
* INPUTS
*   *toolType
*     A pointer to a NULL terminated array of CONST_STRPTR containing the
*     tooltypes provided with the plugin library.
*   *sema
*     Pointer to a semaphore to use to protect access to custom data from
*     ACEpansionPlugin when accessed from subtasks or within your PrefsWindow.
*
* RESULT
*     A pointer to an allocated and initialized plugin data structure.
*
* SEE ALSO
*   DeletePlugin
*   ActivatePlugin
*   GetPrefsPlugin
*
*****************************************************************************
*
*/

ACEPANSION_API
struct ACEpansionPlugin * CreatePlugin(UNUSED CONST_STRPTR *toolTypes, UNUSED struct SignalSemaphore *sema)
{
    // We are allocating an extended plugin structure to add our custom stuff
    struct PluginData *plugin = (struct PluginData *)AllocVec(sizeof(struct PluginData), MEMF_PUBLIC|MEMF_CLEAR);

    if(plugin)
    {
        // Initialize plugin generic part
        plugin->common.ap_APIVersion     = API_VERSION;
        plugin->common.ap_Flags          = ACE_FLAGSF_ACTIVE_RESET
                                         | ACE_FLAGSF_ACTIVE_READMEM
                                         | ACE_FLAGSF_ACTIVE_WRITEMEM
                                         | ACE_FLAGSF_ACTIVE_WRITEIO;
        plugin->common.ap_Title          = GetString(MSG_TITLE);
        plugin->common.ap_HelpFileName   = LIBNAME".guide";
        plugin->common.ap_ToggleMenuName = GetString(MSG_MENU_TOGGLE);
        plugin->common.ap_PrefsMenuName  = NULL;
        plugin->common.ap_PrefsWindow    = NULL;

        // Initialize plugin specifics
        plugin->map = 0x0000;
        plugin->adr = NVRAM_OFF;
    }

    return (struct ACEpansionPlugin *)plugin;
}



/****** nova.acepansion/DeletePlugin ****************************************
*
* NAME
*   DeletePlugin -- (V1) -- GUI
*
* SYNOPSIS
*   VOID DeletePlugin(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Delete a plugin data structure. This function is usually called when ACE
*   is exiting.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to delete.
*
* RESULT
*
* SEE ALSO
*   CreatePlugin
*   ActivatePlugin
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID DeletePlugin(struct ACEpansionPlugin *plugin)
{
    FreeVec(plugin);
}



/****** nova.acepansion/ActivatePlugin **************************************
*
* NAME
*   ActivatePlugin -- (V1) -- GUI
*
* SYNOPSIS
*   BOOL ActivatePlugin(struct ACEpansionPlugin *plugin, BOOL status)
*
* FUNCTION
*   This function is called everytime ACE wants to activate or disactivate your plugin.
*   While you are disactivated, no other function (except Delete) will the called by ACE.
*   Please note that a plugin is always disactivated by ACE prior to DeletePlugin().
*
* INPUTS
*   activate
*     New activation status requested by ACE.
*
* RESULT
*   Actual activation status (you may fail at activation for some reason, but you
*   should never fail at disactivation request).
*
* SEE ALSO
*   CreatePlugin
*   DeletePlugin
*
*****************************************************************************
*
*/

ACEPANSION_API
BOOL ActivatePlugin(UNUSED struct ACEpansionPlugin *plugin, BOOL activate)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;
    BPTR nvram;

    if((nvram = Open(NVRAM_FILE, MODE_READWRITE)))
    {
        if(activate)
        {
            Read(nvram, myPlugin->nvram, NVRAM_SIZE);
        }
        else
        {
            Write(nvram, myPlugin->nvram, NVRAM_SIZE);
        }
        Close(nvram);
    }

    return activate;
}



/****** nova.acepansion/GetPrefsPlugin **************************************
*
* NAME
*   GetPrefsPlugin -- (V5) -- GUI
*
* SYNOPSIS
*   STRPTR * GetPrefsPlugin(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Function that is used by ACE to know about the current preferences of the
*   plugins in order to save them.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   pool
*     Memory pool to use to allocate the tooltypes.
*
* RESULT
*   Pointer to a NULL terminated string array containing the tooltypes or NULL
*   when no tooltypes.
*
* SEE ALSO
*   CreatePlugin
*
*****************************************************************************
*
*/

ACEPANSION_API
STRPTR * GetPrefsPlugin(UNUSED struct ACEpansionPlugin *plugin, UNUSED APTR pool)
{
    return NULL;
}



/****** nova.acepansion/Reset ***********************************************
*
* NAME
*   Reset -- (V1) -- EMU
*
* SYNOPSIS
*   VOID Reset(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Function that is called everytime ACE is issuing a bus reset.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*
* RESULT
*
* SEE ALSO
*   Emulate
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Reset(UNUSED struct ACEpansionPlugin *plugin)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    myPlugin->adr = NVRAM_OFF;
}



/****** nova.acepansion/Emulate *********************************************
*
* NAME
*   Emulate -- (V1) -- EMU
*
* SYNOPSIS
*   VOID Emulate(struct ACEpansionPlugin *plugin, ULONG *signals)
*
* FUNCTION
*   Function that is called at each microsecond of the emulation step inside ACE.
*   Note: Implement this function only if is it really required for your plugin because it
*   is obviously CPU intensive.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   *signals
*     Pointer to the I/O signals from the expansion port you can use or alter
*     depending on what you are emulating.
*     The signals bits can be decoded using the following flags:
*       ACE_SIGNALF_INT -- (V1)
*         Maskable interrupt; corresponding to /INT signal from CPC expansion
*         port.
*       ACE_SIGNALF_NMI -- (V3)
*         Non-maskable interrupt; corresponding to /NMI signal from CPC
*         expansion port.
*       ACE_SIGNALF_LPEN -- (V4)
*         Light pen trigger; corresponding to /LPEN signal from CPC expansion
*         port.
*       ACE_SIGNALF_BUS_RESET -- (V6)
*         Bus reset trigger; correspond to /BUSRESET signal from CPC expansion
*         port.
*       ACE_SIGNALF_NOT_READY -- (V6)
*         Not ready trigger; correspond to READY signal from CPC expansion port.
*
* RESULT
*
* SEE ALSO
*   Reset
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Emulate(UNUSED struct ACEpansionPlugin *plugin, UNUSED ULONG *signals)
{
}



/****** nova.acepansion/WriteIO *********************************************
*
* NAME
*   WriteIO -- (V1) -- EMU
*
* SYNOPSIS
*   VOID WriteIO(struct ACEpansionPlugin *plugin, USHORT port, UBYTE value)
*
* FUNCTION
*   Function that is called everytime Z80 is performing a I/O port write operation.
*   Note: Implement this function only if is it really required for your plugin because it
*   is obviously CPU intensive.
*
* INPUTS
*   port
*     Z80 port on with the I/O write operation was issued.
*   value
*     Value that was issued on the port.
*
* RESULT
*
* SEE ALSO
*   ReadIO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID WriteIO(struct ACEpansionPlugin *plugin, USHORT port, UBYTE value)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    if(port == 0xfe82)
    {
        if((value & 0x0c) != 0x08)
        {
            myPlugin->adr = NVRAM_OFF;
        }
        else
        {
            myPlugin->adr = (value & 3) * 0x2000;
            myPlugin->map = (value & 0xe0) << 8;
        }
    }
}



/****** nova.acepansion/ReadIO **********************************************
*
* NAME
*   ReadIO -- (V1) -- EMU
*
* SYNOPSIS
*   VOID ReadIO(struct ACEpansionPlugin *plugin, USHORT port, UBYTE *value)
*
* FUNCTION
*   Function that is called everytime Z80 is performing a I/O port read operation.
*   Note: Implement this function only if is it really required for your plugin because it
*   is obviously CPU intensive.
*
* INPUTS
*   port
*     Z80 port on with the I/O read operation was issued.
*   *value
*     Value to be returned.
*
* RESULT
*
* SEE ALSO
*   WriteIO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID ReadIO(UNUSED struct ACEpansionPlugin *plugin, UNUSED USHORT port, UNUSED UBYTE *value)
{
}



/****** nova.acepansion/WriteMem ********************************************
*
* NAME
*  WriteMem -- (V6) -- EMU
*
* SYNOPSIS
*  BOOL WriteMem(struct ACEpansionPlugin *plugin, USHORT address, UBYTE value)
*
* FUNCTION
*  Function that is called everytime Z80 is performing a memory write
*  operation.
*
* INPUTS
*  *plugin
*    Pointer to the plugin data structure to use.
*  address
*    Z80 address on which the memory write operation was issued.
*  value
*    Value that has to be written.
*
* RESULT
*  TRUE if the plugins actually cougth the memory write operation so that
*  internal memory will be left unchanged (/RAMDIS is set to 0).
*
* NOTES
*  Implement this function only if is it really required for your plugin
*  because it is obviously CPU intensive.
*
* SEE ALSO
*  ReadMem()
*
*****************************************************************************
*
*/

ACEPANSION_API
BOOL WriteMem(struct ACEpansionPlugin *plugin, USHORT address, UBYTE value)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    if(myPlugin->adr != NVRAM_OFF)
    {
        if((address & 0xe000) == myPlugin->map)
        {
            LONG idx = myPlugin->adr + (address & 0x1fff);

            // We don't emulate write clock (only mode can be changed)
            if(idx <= IDX_CLOCK_MODE)
            {
                myPlugin->nvram[idx] = value;

                // Freeze the current clock in read mode
                if((myPlugin->nvram[IDX_CLOCK_MODE] & 0xc0) == 0x40)
                {
                    updateClockRegisters(myPlugin);
                }
            }

            return TRUE;
        }
    }

    return FALSE;
}



/****** nova.acepansion/ReadMem *********************************************
*
* NAME
*  ReadMem -- (V6) -- EMU
*
* SYNOPSIS
*  VOID ReadMem(struct ACEpansionPlugin *plugin, USHORT address, UBYTE *value, BOOL opcodeFetch) -- (V7)
*  BOOL ReadMem(struct ACEpansionPlugin *plugin, USHORT address, UBYTE *value, BOOL opcode) ** DEPRECATED in V7 **

* FUNCTION
*  Function that is called everytime Z80 is performing a memory read
*  operation.
*
* INPUTS
*  *plugin
*    Pointer to the plugin data structure to use.
*  address
*    Z80 address on which memory read operation was issued.
*  *value
*    Value to be returned.
*    Starting with V7, it contains by default the value that might be read by
*    the CPC if the plugin won't catch the memory read, so that a plugin can
*    only snif the bus.
*  opcodeFetch
*    Boolean which is TRUE when the memory read operation is related to
*    Z80 opcode fetching (/M1 signal). If FALSE, then this is a regular
*    read operation during opcode execution.
*
* RESULT
*  ** DEPRECATED in V7 **
*  TRUE if the plugins actually cougth the memory read operation so that
*  internal memory won't be read (both /ROMDIS and /RAMDIS are set to 0).
*
* NOTES
*  Implement this function only if is it really required for your plugin
*  because it is obviously CPU intensive.
*
* SEE ALSO
*  WriteMem()
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID ReadMem(struct ACEpansionPlugin *plugin, USHORT address, UBYTE *value, UNUSED BOOL opcodeFetch)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    if(myPlugin->adr != NVRAM_OFF)
    {
        if((address & 0xe000) == myPlugin->map)
        {
            LONG idx = myPlugin->adr + (address & 0x1fff);

            if(idx >= IDX_CLOCK_MODE)
            {
                // Frozen in read mode (0x40) or write mode (0x80)
                // Note: write mode is not emulated
                if((myPlugin->nvram[IDX_CLOCK_MODE] & 0xc0) == 0)
                {
                    updateClockRegisters(myPlugin);
                }
            }

            *value = myPlugin->nvram[idx];
        }
    }
}



/****** nova.acepansion/GetAudio ********************************************
*
* NAME
*   GetAudio -- (V1) -- EMU
*
* SYNOPSIS
*   VOID GetAudio(struct ACEpansionPlugin *plugin, SHORT *leftSample, SHORT *rightSample)
*
* FUNCTION
*   Function to be used when your plugin needs to mix an audio output with the one
*   from ACE.
*   It is called at each PSG emulation step, which means at 125KHz.
*
* INPUTS
*   *leftSample
*     Pointer to store the left audio sample to be mixed with ACE audio.
*   *rightSample
*     Pointer to store the right audio sample to be mixed with ACE audio.
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID GetAudio(UNUSED struct ACEpansionPlugin *plugin, UNUSED SHORT *leftSample, UNUSED SHORT *rightSample)
{
}



/****** nova.acepansion/Printer *********************************************
*
* NAME
*   Printer -- (V1) -- EMU
*
* SYNOPSIS
*   VOID WritePrinter(struct ACEpansionPlugin *plugin, UBYTE data, BOOL strobe, BOOL *busy)
*
* FUNCTION
*   Function that is called everytime the CPC is accessing the printer port.
*
* INPUTS
*   data
*     8 bits written value (note that CPC is limited to 7 bits values, only Amstrad Plus
*     can actually provide the 8th bit).
*   strobe
*     Status of the /STROBE signal from the printer port.
*   *busy
*     Busy signal value that can be altered.
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Printer(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE data, UNUSED BOOL strobe, UNUSED BOOL *busy)
{
}



/****** nova.acepansion/Joystick ********************************************
*
* NAME
*   Joystick -- (V2) -- EMU
*
* SYNOPSIS
*   VOID Joystick(struct ACEpansionPlugin *plugin, BOOL com1, BOOL com2, UBYTE *ioData)
*
* FUNCTION
*   Function which is called everytime the CPC is accessing the joystick port.
*   Please note that the joystick port in R/W but you cannot know if the CPC
*   is actually reading or writing to it.
*
* INPUTS
*   com1
*     If com1 is active then the CPC is requesting access to joystick 0.
*   com2
*     If com2 is active then the CPC is requesting access to joystick 1.
*   *ioData
*     If the CPC is reading the joystick port (regular case) ioData will be set
*     to 0xff and you are supposed to set to 0 the bits corresponding to the
*     requested joystick (you should use the ACE_IODATAF_JOYSTICK_XXX defines).
*     Please note the additional ACE_IODATAF_JOYSTICK_PAUSE which is not
*     existing on CPC but that you could use to map the play/pause button of
*     a joystick used in ACE on the 'P' key of the keyboard (or the GX4000
*     pause button).
*     If the CPC is writing the joystick port the ioData will contain the
*     written value to the port and modifying it will have not effect (on a
*     real CPC it could simply destroy the PSG chipset!).
*
* RESULT
    To emulate the analog joystick port of the Amstrad Plus and the GX-4000,
    this function should be used together with AnalogInput().
*
* SEE ALSO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Joystick(UNUSED struct ACEpansionPlugin *plugin, UNUSED BOOL com1, UNUSED BOOL com2, UNUSED UBYTE *ioData)
{
}



/****** nova.acepansion/AnalogInput *****************************************
*
* NAME
*   AnalogInput -- (V7) -- EMU
*
* SYNOPSIS
*   VOID AnalogInput(struct ACEpansionPlugin *plugin, UBYTE channel, UBYTE *value)
*
* FUNCTION
*   Function which is called everytime the CPC is accessing the analog joystick
*   port of the Amstrad Plus or the GX-4000.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   channel
*     Analog channel number (from 0 to 3).
*   *value
*     Pointer where to store the 6-bit wide value of the data on the channel.
*
* RESULT
*
* NOTES
*   To emulate the analog joystick port of the Amstrad Plus and the GX-4000,
*   this function should be used together with Joystick() which is providing
*   fire buttons (they are shared between both analog and digital joystick
*   ports).
*
* SEE ALSO
*   Joystick()
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID AnalogInput(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE channel, UNUSED UBYTE *value)
{
}



/****** nova.acepansion/AcknowledgeInterrupt ********************************
*
* NAME
*   AcknowledgeInterrupt -- (V1) -- EMU
*
* SYNOPSIS
*   VOID AcknowledgeInterrupt(struct ACEpansionPlugin *plugin, UBYTE *ivr)
*
* FUNCTION
*   Function which is called everytime the Z80 is entering an interrupt.
*
* INPUTS
*   *ivr
*     IVR could be set to the desired value (Interrupt Vector Register).
*
* RESULT
*
* SEE ALSO
*   ReturnInterrupt
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID AcknowledgeInterrupt(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE *ivr)
{
}



/****** nova.acepansion/ReturnInterrupt *************************************
*
* NAME
*   ReturnInterrupt -- (V3) -- EMU
*
* SYNOPSIS
*   VOID ReturnInterrupt(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Function which is called everytime the Z80 is returning from an interrupt
*   using RETI instruction.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*   AcknowledgeInterrupt
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID ReturnInterrupt(UNUSED struct ACEpansionPlugin *plugin)
{
}



/****** nova.acepansion/Cursor **********************************************
*
* NAME
*   Cursor -- (V1) -- EMU
*
* SYNOPSIS
*   VOID Cursor(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Function which is called everytime the cursor signal from the CRTC is
*   active.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Cursor(UNUSED struct ACEpansionPlugin *plugin)
{
}



/****** nova.acepansion/HostGamepadList *************************************
*
* NAME
*   HostGamepadList -- (V7) -- GUI

* SYNOPSIS
*   VOID HostGamepadList(struct ACEpansionPlugin *plugin, STRPTR *list)
*
* FUNCTION
*   This function is called everytime a gamepad in plugged or unplugged from
*   the host so that the plugin can know about the available devices. Index
*   from HostGamepadEvent() will refer to this list.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   list
*     NULL terminated array of strings containing the human readable names of
*     the plugged gamepads, the first one being the localized string for "No
*     joystick". Please note that the contents of this list remains valid
*     until the function is invoked again, so that you can use it safely in
*     your code without duplication.
*
* RESULT
*
* NOTES
*
* SEE ALSO
*   HostGamepadList()
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostGamepadList(UNUSED struct ACEpansionPlugin *plugin, UNUSED STRPTR *list)
{
}



/****** nova.acepansion/HostGamepadEvent ************************************
*
* NAME
*   HostGamepadEvent -- (V7) -- GUI
*
* SYNOPSIS
*   VOID HostGamepadEvent(struct ACEpansionPlugin *plugin, UBYTE index, USHORT buttons, BYTE ns, BYTE ew, BYTE lx, BYTE ly, BYTE rx, BYTE ry)
*
* FUNCTION
*   This function is called everytime the state of a host's gamepad state is
*   changing. It let you handle both analogic and digital devices.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   index
*     Index of the gamepad in gamepad list.
*   buttons
*     State of the gamepad buttons.
*     Buttons bits can be decoded using the ACE_HOSTGAMEPADF_BUTTON_XXX flags.
*   ns
*     Horizontal position of directional buttons from -128 to +127.
*   ew
*     Vertical position of directional buttons from -128 to +127.
*   lx
*     Horizontal position of left stick from -128 to +127.
*   ly
*     Vertical position of left stick from -128 to +127.
*   rx
*     Horizontal position of right stick from -128 to +127.
*   ry
*     Vertical position of right stick from -128 to +127.
*
* RESULT
*
* NOTES
*
* SEE ALSO
*   HostGamepadList()
*   libraries/acepansion_plugin.h
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostGamepadEvent(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE index, UNUSED USHORT buttons, UNUSED BYTE ns, UNUSED BYTE ew, UNUSED BYTE lx, UNUSED BYTE ly, UNUSED BYTE rx, UNUSED BYTE ry)
{
}



/****** nova.acepansion/HostMouseEvent **************************************
*
* NAME
*   HostMouseEvent -- (V7) -- GUI

* SYNOPSIS
*   VOID HostMouseEvent(struct ACEpansionPlugin *plugin, UBYTE buttons, SHORT deltaX, SHORT deltaY)

* FUNCTION
*   Function that is called everytime a host's mouse event occured

* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   buttons
*     State of the mouse buttons.
*     The mouse buttons bits can be decoded using the following flags:
*       ACE_HOSTMOUSEF_BUTTON_MAIN
*         State of main mouse button.
*       ACE_HOSTMOUSEF_BUTTON_SECOND
*         State of second mouse button.
*       ACE_HOSTMOUSEF_BUTTON_MIDDLE
*         State of middle mouse button.
*   deltaX
*     Horizontal mouse move until previous event.
*   deltaY
*     Vertical mouse move until previous event.
*
* RESULT
*
* NOTES
*
* SEE ALSO
      libraries/acepansion_plugin.h
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostMouseEvent(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE buttons, UNUSED SHORT deltaX, UNUSED SHORT deltaY)
{
}



/****** nova.acepansion/HostLightDeviceDiodePulse ***************************
*
* NAME
*   HostLightDeviceDiodePulse -- (V7) -- EMU
*
* SYNOPSIS
*   VOID HostLightDeviceDiodePulse(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   This function is called at each photo-diode pulse, when the beam from
*   the screen monitor is just in front of the light device photo-diode.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*   HostLightDeviceButton
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostLightDeviceDiodePulse(UNUSED struct ACEpansionPlugin *plugin)
{
}



/****** nova.acepansion/HostLightDeviceButton *******************************
*
* NAME
*   HostLightDeviceButton -- (V7) -- GUI
*
* SYNOPSIS
*   VOID HostLightDeviceButton(struct ACEpansionPlugin *plugin, BOOL pressed)
*
* FUNCTION
*   This function is called everytime the state of the light device button
*   is changing.
*
* INPUTS
*   pressed
*     New state of the light device button.
*
* RESULT
*
* NOTE
*   When a light device plugin is activated, ACE is mapping this button on
*   mouse main button, so that on-screen click will emulate light device
*   button.
*
* SEE ALSO
*   HostLightDeviceDiodePulse
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostLightDeviceButton(UNUSED struct ACEpansionPlugin *plugin, UNUSED BOOL pressed)
{
}




/*
** Helper functions
*/

static VOID updateClockRegisters(struct PluginData *myPlugin)
{
#if defined(__amigaos__)
    struct timeval timeVal;;
    struct ClockData clockData;

    GetSysTime(&timeVal);
    Amiga2Date(timeVal.tv_secs, &clockData);

    myPlugin->nvram[IDX_CLOCK_SEC]   = ToBCD[clockData.sec];
    myPlugin->nvram[IDX_CLOCK_MIN]   = ToBCD[clockData.min];
    myPlugin->nvram[IDX_CLOCK_HOUR]  = ToBCD[clockData.hour];
    myPlugin->nvram[IDX_CLOCK_WDAY]  = ToBCD[ToMDay[clockData.wday]];
    myPlugin->nvram[IDX_CLOCK_MDAY]  = ToBCD[clockData.mday];
    myPlugin->nvram[IDX_CLOCK_MONTH] = ToBCD[clockData.month];
    myPlugin->nvram[IDX_CLOCK_YEAR]  = ToBCD[clockData.year>2000?clockData.year-2000:0];
#elif defined(__HAIKU__)
	time_t nowTime = time(NULL);
	struct tm now;
	localtime_r(&nowTime, &now);
    myPlugin->nvram[IDX_CLOCK_SEC]   = ToBCD[now.tm_sec];
    myPlugin->nvram[IDX_CLOCK_MIN]   = ToBCD[now.tm_min];
    myPlugin->nvram[IDX_CLOCK_HOUR]  = ToBCD[now.tm_hour];
    myPlugin->nvram[IDX_CLOCK_WDAY]  = ToBCD[ToMDay[now.tm_wday]];
    myPlugin->nvram[IDX_CLOCK_MDAY]  = ToBCD[now.tm_mday];
    myPlugin->nvram[IDX_CLOCK_MONTH] = ToBCD[now.tm_mon + 1];
    myPlugin->nvram[IDX_CLOCK_YEAR]  = ToBCD[now.tm_year>100?now.tm_year-100:0];
#else
#error FixMe!
#endif
}

